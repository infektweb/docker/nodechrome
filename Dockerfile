FROM node:lts

WORKDIR /app
RUN apt-get update && apt-get upgrade -y && apt-get install apt-transport-https; \
    echo "deb https://dl.bintray.com/sobolevn/deb git-secret main" | \
    tee -a /etc/apt/sources.list && wget -qO - \
    https://api.bintray.com/users/sobolevn/keys/gpg/public.key | \
    apt-key add - && apt-get update && apt-get install -y git-secret && \
    wget --quiet https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb && \
    apt-get -y install ./google-chrome-stable_current_amd64.deb && \
    rm ./google-chrome-stable_current_amd64.deb

RUN for i in gnupg ssh; do mkdir -m 700 -p ~/.$i/; done
COPY gpg.conf ~/.gnupg/gpg-agent.conf

CMD ["yarn"]
